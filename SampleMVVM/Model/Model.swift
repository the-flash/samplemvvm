//
//  Model.swift
//  SampleMVVM
//
//  Created by ADMIN on 20/02/19.
//  Copyright © 2019 ADMIN. All rights reserved.
//

import UIKit

//https://next.json-generator.com/api/json/get/4kobjnVE8

/****************************** Student Name Model *********************************/
class Model: NSObject {

    
}

/****************************** JSON Parsing Model *********************************/

struct StudentData: Codable {
    let studentdetails: [Studentdetail]
}
struct Studentdetail: Codable {
    let school, studentname, studentdetailClass, board: String
    
    enum CodingKeys: String, CodingKey {
        case school, studentname
        case studentdetailClass = "class"
        case board
    }
}
