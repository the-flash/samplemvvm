//
//  ViewModel.swift
//  SampleMVVM
//
//  Created by ADMIN on 20/02/19.
//  Copyright © 2019 ADMIN. All rights reserved.
//

import UIKit

class ViewModel: NSObject {
    
    func forStudentName(completionBlock: @escaping (Array<String>, Array<String>) -> Void) {
        var CstmrName = Array<String>()
        var CstmrClass = Array<String>()
        
        let task = URLSession.shared.dataTask(with: URL(string: DefaultData.sharedInstance.Url)!){ data, response, error in
            
            guard let jsonData = data else { return }
            
            do {
                let studentData = try JSONDecoder().decode(StudentData.self, from: jsonData)
                for index in studentData.studentdetails{
                    CstmrName.append(index.studentname)
                    CstmrClass.append(index.studentdetailClass)
                }
                completionBlock(CstmrName, CstmrClass)
                
            }catch let error  {
                print(error)
            }
        }
        task.resume()
    }
}
