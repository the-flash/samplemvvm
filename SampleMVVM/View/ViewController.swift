//
//  ViewController.swift
//  SampleMVVM
//
//  Created by ADMIN on 20/02/19.
//  Copyright © 2019 ADMIN. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var NameLabel: UILabel!
    @IBOutlet var ClassLabel: UILabel!
    
    var viewModel: ViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = ViewModel()
        viewModel.forStudentName{ _name, _class in
            DispatchQueue.main.async {
                self.NameLabel.text = _name[0]
                self.ClassLabel.text = _class[0]
            }
        }
    }
}

